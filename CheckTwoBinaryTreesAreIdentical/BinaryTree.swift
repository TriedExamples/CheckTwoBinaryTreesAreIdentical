//
//  BinaryTree.swift
//  CheckTwoBinaryTreesAreIdentical
//
//  Created by vishnusankar on 19/06/18.
//  Copyright © 2018 vishnusankar. All rights reserved.
//

import Foundation

class Node {
    var leftNode : Node?
    var rightNode : Node?
    let value : Int
    
    init(value : Int) {
        self.value = value
    }
}

class BinaryTree {

    let rootNode : Node
    
    init(rootNode: Node) {
        self.rootNode = rootNode
    }
    
    func addNode(traversNode : Node, newNode : Node) {
        
        if newNode.value > traversNode.value {
            if traversNode.rightNode == nil {
                traversNode.rightNode = newNode
            }else {
                addNode(traversNode: traversNode.rightNode!, newNode: newNode)
            }
        }else {
            if traversNode.leftNode == nil {
                traversNode.leftNode = newNode
            }else {
                addNode(traversNode: traversNode.leftNode!, newNode: newNode)
            }
        }
    }
    
    static func checkBothTreeIsIdentical(firstRootNode : Node, secondRootNode : Node) -> Bool{
        if firstRootNode.value != secondRootNode.value {
            return false
        }
        
        if firstRootNode.leftNode != nil && secondRootNode.leftNode != nil {
            return self.checkBothTreeIsIdentical(firstRootNode: firstRootNode.leftNode!, secondRootNode: secondRootNode.leftNode!)
        }
        
        if firstRootNode.rightNode != nil && secondRootNode.rightNode != nil {
            return self.checkBothTreeIsIdentical(firstRootNode: firstRootNode.rightNode!, secondRootNode: secondRootNode.rightNode!)
        }
        return true
    }
    
    
}
